#include "stdafx.h"
#include "header.h"
#include <iostream>
#include <ctime>


int main()
{
	Graph<int, string>A(6); 

							//A.setDataVRandom();

	A.setVbyHerindexinGraf(1, 65, "Samara");
	A.setVbyHerindexinGraf(2, 52, "Moscow");
	A.setVbyHerindexinGraf(3, 73, "New York");
	A.setVbyHerindexinGraf(4, 54, "York");
	A.setVbyHerindexinGraf(5, 43, "St.Petersburg");
	A.setVbyHerindexinGraf(6, 45, "Los Angel's");

	A.showVertex();
	cout << endl;
	try
	{
		cout << A.searchVbyKey("Samara") << endl;
		cout << A.searchVbyKey("London") << endl;
	}
	catch (...)
	{
		cout << "Received exemption" << endl;
	}
	A.showMatrixConnection();
	A.addVertex(35, "London");
	A.addConnectionAll();
	cout << endl;
	A.showVertex();
	cout << endl;
	A.showMatrixConnection();
	bool fl = A.directedGraph();
	if (fl == true)
		cout << "Ort";
	else
		cout << "Unort";
	cout << endl;
	A.delVertex(3);
	A.showVertex();
	fl = A.existencePath(2, 4);
	if (fl == true)
		cout << "Yes";
	else
		cout << "No";
	cout << endl;
	A.showMatrixConnection();
	A.connectivityGraph();
	A.clear();
	system("pause");
	return 0;
}