#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;

template <class T, typename F>
class Graph
{
	struct Vertex
	{
		F key;
		T data;
		Vertex();
		Vertex(const F& a, const T& b);
		Vertex(const Vertex& a);
		friend ostream& operator<<(ostream& os, const Vertex& dt)//Перегрузка вывода;
		{
			os << dt.data;
			return os;
		};
	};
	int numofV;
	Vertex* arrayV;
	int** AdjacencyMatrix;
	bool noConnectionGraph();//Определяет связный граф или нет
	bool strongConnectionGraph();//Определяет тип связности

public:

	Graph(int sizeV); 
	T &operator[] (int index); 
	bool directedGraph(); //Определяет напрвленость графа
	void connectivityGraph();//Выводит инфорамацию о связности графа
	bool existencePath(int Vertex1, int Vertex2); //Определяет существует ли путь между 2 вершинами
	int numberofVertex();//Возвращает кол-во вершин
	void setDataV();// Утановить значения вершины с клавиатуры
	void setDataVRandom(); //Устанавливает значения вершин рандомно
	void showVertex(); //Отображает вершины
	void showMatrixConnection();//Отображает матрицу связей(матрицу смежности)
	void addVertex(const T& value, const F& k); //Добавляет вершину
	void addConnection(int index);//Добавляет связи в вершине
	void addConnectionAll(); //Обертка над предидущей функцией для заполнения связей всех вершин
	void delConnection(int index);//Удаление связей вершины
	void clearConnection();//Очистка всех связей
	void clearVertex();//Очитска значений вершин
	void clear();//Выполняет две предидущие функции
	void delVertex(int index);//Уничтожение вершины графа
	void transOrttoUnort();//Превращение ортографа в обычный
	void setVbyHerindexinGraf(int index, const T& value, const F& k);//Установка\изменение значений вершины по ее индексу
	Vertex searchVbyKey(const F& key);//Поиск вершины по ключу

};

template <class T, typename F>
Graph<T, F>::Graph(int sizeV)
{
	numofV = sizeV;
	arrayV = new Vertex[numofV];

	AdjacencyMatrix = new int*[numofV];
	for (int i = 0; i<numofV; i++)
	{
		AdjacencyMatrix[i] = new int[numofV];
	}

	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			AdjacencyMatrix[i][j] = 0;
		}
}
template <class T, typename F>
T &Graph<T, F>::operator [](int index)
{
	if (index >= 0 && index<numofV)
	{
		return arrayV[index];
	}
	else
		throw "Out of graph's element.";
}
template <class T, typename F>
int Graph<T, F>::numberofVertex()
{
	return numofV;
}
template <class T, typename F>
void Graph<T, F>::setDataV()
{
	for (int i = 0; i<numofV; i++)
	{
		cout << "Vertex " << i + 1 << ": ";
		cin >> arrayV[i].key;
		cin >> arrayV[i].data;
	}
}
template <class T, typename F>
void Graph<T, F>::setDataVRandom()
{
	srand(unsigned(time(nullptr)));
	for (int i = 0; i<numberofVertex(); i++)
	{
		arrayV[i] = Vertex(i + 1, rand() % 100);
	}
}
template <class T, typename F>
void Graph<T, F>::showVertex()
{
	for (int i = 0; i<numofV; i++)
	{
		cout << "Vertex " << i + 1 << ": " << arrayV[i].key << " " << arrayV[i].data << endl;
	}
}
template <class T, typename F>
void Graph<T, F>::showMatrixConnection()
{
	for (int i = 0; i<numberofVertex(); i++)
	{
		for (int j = 0; j<numberofVertex(); j++)
		{
			cout << AdjacencyMatrix[i][j] << " ";
		}
		cout << endl;
	}
}
template <class T, typename F>
void Graph<T, F>::addVertex(const T& value, const F& k)
{
	Vertex* buff = new Vertex[numofV];
	for (int i = 0; i<numofV; i++)
	{
		buff[i] = arrayV[i];
	}
	delete[] arrayV;
	arrayV = new Vertex[numofV + 1];
	for (int i = 0; i<numofV; i++)
	{
		arrayV[i] = buff[i];
	}
	arrayV[numofV].key = k;
	arrayV[numofV].data = value;
	numofV++;
	int** bufferMatrix = new int*[numofV];
	for (int i = 0; i<numofV; i++)
	{
		bufferMatrix[i] = new int[numofV];
	}

	for (int i = 0; i< numofV - 1; i++)
		for (int j = 0; j< numofV - 1; j++)
		{
			bufferMatrix[i][j] = AdjacencyMatrix[i][j];
		}
	for (int i = 0; i < numofV - 1; i++)
		delete AdjacencyMatrix[i];
	delete[] AdjacencyMatrix;

	AdjacencyMatrix = new int*[numofV];
	for (int i = 0; i<numofV; i++)
	{
		AdjacencyMatrix[i] = new int[numofV];
	}

	for (int i = 0; i<numofV - 1; i++)
		for (int j = 0; j<numofV - 1; j++)
		{
			AdjacencyMatrix[i][j] = bufferMatrix[i][j];
		}
	for (int i = 0; i<numofV; i++)
	{
		AdjacencyMatrix[numofV - 1][i] = 0;
		AdjacencyMatrix[i][numofV - 1] = 0;
	}
	for (int i = 0; i < numofV - 1; i++)
		delete[] bufferMatrix[i];
	delete[] bufferMatrix;
}
template <class T, typename F>
void Graph<T, F>::addConnection(int index)
{
	if (index > 0 && index <= numofV)
	{
		char ch = '0';

		cout << "Connection with " << index << " vertex: ";
		while (ch != '\n')
		{
			int index2;
			cin >> index2;
			if ((index2>0) && (index2 <= numofV))
			{
				AdjacencyMatrix[index - 1][index2 - 1] = 1;
			}
			ch = cin.get();
			if (ch == '\n')
				break;
		}
	}
}
template <class T, typename F>
void Graph<T, F>::addConnectionAll()
{
	for (int i = 1; i <= numofV; i++)
	{
		addConnection(i);
	}
}
template <class T, typename F>
void Graph<T, F>::delConnection(int index)
{
	if (index > 0 && index <= numofV)
	{
		char ch = '0';

		cout << "Delete connection with " << index << " vertex: ";
		while (ch != '\n')
		{
			int index2;
			cin >> index2;
			if ((index2>0) && (index2 <= numofV))
			{
				AdjacencyMatrix[index - 1][index2 - 1] = 0;
			}
			ch = cin.get();
			if (ch == '\n')
				break;
		}
	}

}
template <class T, typename F>
void Graph<T, F>::clearConnection()
{
	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			AdjacencyMatrix[i][j] = 0;
		}
}
template <class T, typename F>
void Graph<T, F>::clearVertex()
{
	for (int i = 0; i<numofV; i++)
		arrayV[i].data = 0;
}
template <class T, typename F>
void Graph<T, F>::clear()
{
	clearConnection();
	clearVertex();
}
template<class T, typename F>
void Graph<T, F>::delVertex(int index)
{
	index--;
	Vertex* bufferV = new Vertex[numofV - 1];
	for (int i = 0; i<numofV - 1; i++)
	{
		if (i < index)
			bufferV[i] = arrayV[i];
		else
			bufferV[i] = arrayV[i + 1];
	}
	delete[] arrayV;
	arrayV = new Vertex[numofV - 1];
	for (int i = 0; i<numofV - 1; i++)
	{
		arrayV[i] = bufferV[i];
	}
	delete[] bufferV;
	int** bufferMatrix = new int*[numofV - 1];
	for (int i = 0; i<numofV - 1; i++)
	{
		bufferMatrix[i] = new int[numofV - 1];
	}
	for (int i = 0; i<numofV - 1; i++)
		for (int j = 0; j < numofV - 1; j++)
		{
			if (i < index&&j < index)
				bufferMatrix[i][j] = AdjacencyMatrix[i][j];
			else
			{
				if (i >= index&&j < (numofV - 1))
					bufferMatrix[i][j] = AdjacencyMatrix[i + 1][j];
				if (i < (numofV - 1) && j >= index)
					bufferMatrix[i][j] = AdjacencyMatrix[i][j + 1];
				if (i >= index&&j >= index)
					bufferMatrix[i][j] = AdjacencyMatrix[i + 1][j + 1];
			}
		}
	for (int i = 0; i < numofV; i++)
		delete AdjacencyMatrix[i];
	delete[] AdjacencyMatrix;

	AdjacencyMatrix = new int*[numofV - 1];
	for (int i = 0; i<numofV - 1; i++)
	{
		AdjacencyMatrix[i] = new int[numofV - 1];
	}

	for (int i = 0; i<numofV - 1; i++)
		for (int j = 0; j<numofV - 1; j++)
		{
			AdjacencyMatrix[i][j] = bufferMatrix[i][j];
		}
	for (int i = 0; i < numofV - 1; i++)
		delete[] bufferMatrix[i];
	delete[] bufferMatrix;

	numofV = numofV - 1;
}
template <class T, typename F>
bool Graph<T, F>::directedGraph()
{
	int counter = 0;
	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			if (AdjacencyMatrix[i][j] == AdjacencyMatrix[j][i])
				counter++;
		}
	if (counter == numofV*numofV)
		return false;
	else
		return true;
}
template <class T, typename F>
bool Graph<T, F>::existencePath(int id_vert1, int id_vert2)
{
	id_vert1--;
	id_vert2--;
	if (id_vert1 < numofV && id_vert1 >= 0 && id_vert2 < numofV && id_vert2 >= 0)
	{
		int** buffMatr = new int*[numofV];
		for (int i = 0; i<numofV; i++)
		{
			buffMatr[i] = new int[numofV];
		}

		for (int i = 0; i<numofV; i++)
			for (int j = 0; j < numofV; j++)
			{
				buffMatr[i][j] = AdjacencyMatrix[i][j];
			}

		bool end = false;
		int i = id_vert1;
		int j = 0;
		int CounterMotion = 1;

		vector <int> save(1);
		save[0] = id_vert1;

		while (end == false)
		{
			if (buffMatr[i][j] == 1)
			{
				buffMatr[i][j] = 0;
				save.push_back(i);
				i = j;
				j = 0;
				CounterMotion++;
				if (i == id_vert2)
				{
					return true;
					end = true;
				}
			}
			else
				if (buffMatr[i][j] == 0)
				{
					j++;
					if (j == numofV)
					{
						CounterMotion--;
						if (CounterMotion != 0)
						{
							i = save[CounterMotion];
							j = 0;
						}

						else
							if (CounterMotion == 0)
							{
								return false;
								end = true;
							}
					}
				}
		}
		for (int i = 0; i < numofV; i++)
			delete[] buffMatr[i];
		delete[] buffMatr;
	}

	else
		throw "Nonexistent vertices";

	return 0;
}
template <class T, typename F>
void Graph<T, F>::transOrttoUnort()
{
	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			if (AdjacencyMatrix[i][j] == 0)
				AdjacencyMatrix[i][j] = AdjacencyMatrix[j][i];
		}
}
template<class T, typename F>
bool Graph<T, F>::noConnectionGraph()
{
	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			if (AdjacencyMatrix[i][j] != 0)
			{
				return false;
				break;
			}
		}
	return true;
}
template<class T, typename F>
bool Graph<T, F>::strongConnectionGraph()
{
	bool end = false;
	for (int i = 0; i<numofV; i++)
		for (int j = 0; j<numofV; j++)
		{
			if (i == j)
				continue;
			else
				if (AdjacencyMatrix[i][j] == 0)
				{
					end = true;
					break;
				}
		}
	if (end == true)
		return false;
	else
		return true;
}
template<class T, typename F>
void Graph<T, F>::connectivityGraph()
{
	if (noConnectionGraph() == true)
		cout << "Incoherent graph." << endl;
	else
	{
		if (strongConnectionGraph() == true)
			cout << "Strongly connected graph." << endl;
		else
			cout << "Pootly connected graph." << endl;
	}
}
template <class T, typename F>
void Graph<T, F>::setVbyHerindexinGraf(int index, const T& value, const F& k)
{
	arrayV[index - 1].key = k;
	arrayV[index - 1].data = value;
}
template <class T, typename F>
typename Graph<T, F>::Vertex Graph<T, F>::searchVbyKey(const F& key)
{
	bool find = false;
	for (int i = 0; i < numofV; i++)
	{
		if (arrayV[i].key == key)
			return arrayV[i];
	}
	if (find == false)
		throw "Haven't node with that key";
}
template<class T, typename F>
inline Graph<T, F>::Vertex::Vertex()
{
}
template<class T, typename F>
inline Graph<T, F>::Vertex::Vertex(const F & a, const T & b)
{
	key = a;
	data = b;
}
template<class T, typename F>
inline Graph<T, F>::Vertex::Vertex(const Vertex& a)
{
	key = a.key;
	data = a.data;
}
